package sqs

import (
	"context"
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

func CreateQueue(queueName string) {
	// Load config from $HOME/.aws
    cfg, err := config.LoadDefaultConfig(context.TODO(),
   		config.WithRegion("ap-south-1"),
   	)
    if err != nil {
        log.Fatalf("Unable to load SDK config, %v", err)
    }

	svc := sqs.NewFromConfig(cfg)
	res, err := svc.CreateQueue(context.TODO(), &sqs.CreateQueueInput{
		QueueName: &queueName,
		Attributes: map[string]string{
			"FifoQueue":                 "true",
		},
	})

	if err != nil {
		log.Fatalf("%s\n", err.Error())
	}
	
	fmt.Println(res.QueueUrl)
}

// sqs.CreateQueue("first.fifo")
