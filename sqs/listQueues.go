package sqs

import (
	"context"
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

func ListQueues() {
	// Load config from $HOME/.aws
    cfg, err := config.LoadDefaultConfig(context.TODO(),
   		config.WithRegion("ap-south-1"),
   	)
    if err != nil {
        log.Fatalf("unable to load SDK config, %v", err)
    }

	svc := sqs.NewFromConfig(cfg)
	listOfQueues, err := svc.ListQueues(context.TODO(), &sqs.ListQueuesInput{})
	if err != nil {
		fmt.Errorf("%s\n", err.Error())
	}
	
	for _, item :=  range listOfQueues.QueueUrls {
		fmt.Println(item)
	}
}
